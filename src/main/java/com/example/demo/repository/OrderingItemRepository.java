package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.demo.entity.OrderingItem;

public interface OrderingItemRepository extends JpaRepository<OrderingItem,Long> {
	
	public OrderingItem deleteByIdAndOrderingId(long itemId, long orderId);
	
	public OrderingItem findById(long id);
	
	public OrderingItem findByName(String name);
	
	public OrderingItem findByNameAndOrderingId(String name,long id);
	
	public boolean existsByNameAndOrderingId(String name,long id);
	
	public boolean existsByName(String name);
}
