package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.demo.entity.PromoCode;

@Repository
public interface PromoCodeRepository extends JpaRepository<PromoCode, Long> {
	
	public PromoCode findByCode(String code);
	public PromoCode findById(long id);
	public boolean existsByCode(String promotionalCode);

}