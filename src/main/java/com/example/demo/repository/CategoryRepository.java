package com.example.demo.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.demo.entity.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
	
	public Collection<Category> findByDescription(String description);
	
	public List<Category> findByOrderByNameAsc();
	
	public Category findByName(String name);
	public Category findById(long id);
	
}
