package com.example.demo.repository;

import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.example.demo.entity.Ordering;

@Repository
public interface OrderingRepository extends JpaRepository<Ordering, Long> {
	
	public Ordering findById(long id);
	
	public List<Ordering> findByOrderByLocalDateTimeDesc();
	public boolean existsById(long id);
	
	@Query(value = "from Ordering t where local_date_time BETWEEN :startDate AND :endDate")
	public List<Ordering> getAllBetweenDates(LocalDateTime startDate,LocalDateTime endDate);
	
}
