package com.example.demo.repository;

import com.example.demo.dto.OutputItemDTO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Category;
import com.example.demo.entity.Item;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {
	
	public boolean existsById(long id);
	public Item findByName(String name);
	public Item findById(long id);
	public List<Item> findByOrderByNameAsc();
	public List<Item> findByOrderByPopularityDesc();
}
