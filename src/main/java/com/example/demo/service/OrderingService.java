package com.example.demo.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.entity.Item;
import com.example.demo.entity.Ordering;
import com.example.demo.entity.OrderingItem;
import com.example.demo.entity.PromoCode;
import com.example.demo.repository.ItemRepository;
import com.example.demo.repository.OrderingItemRepository;
import com.example.demo.repository.OrderingRepository;
import com.example.demo.repository.PromoCodeRepository;

@Service
public class OrderingService {
	
	@Autowired 
	private OrderingRepository orderingRepository;
	
	@Autowired
	private ItemRepository itemRepository;
	
	@Autowired
	private OrderingItemRepository orderingItemRepository;
	
	@Autowired
	private PromoCodeRepository promoCodeRepository;

	@Transactional
	public Ordering createOrder()  throws Exception{
		LocalDateTime currentTime = LocalDateTime.now();
		BigDecimal total = new BigDecimal("0.0");
		
		Ordering order = new Ordering(currentTime,total,"OPEN"); 
		
		return orderingRepository.save(order);
	}

	public Collection<Ordering> getAll()  throws Exception{
		return orderingRepository.findAll();
	}
	
	public Collection<Ordering> getOrderHistory(LocalDate startDate, LocalDate endDate) throws Exception{
		LocalDateTime stDate = startDate.atTime(00,00,00);
		LocalDateTime eDate = endDate.atTime(23,59,00);
		Collection<Ordering> orderingList = orderingRepository.getAllBetweenDates(stDate,eDate);
		ArrayList<Ordering> notOpenOrderingList = new ArrayList<Ordering>();
		for(Ordering ordering: orderingList) {
			if(!ordering.getStatus().equalsIgnoreCase("OPEN")) {
				notOpenOrderingList.add(ordering);
			}
		}
		// order by the most recent order
		Comparator<Ordering> compareByDate = (Ordering o1, Ordering o2) -> o1.getLocalDateTime().compareTo(o2.getLocalDateTime());
		Collections.sort(notOpenOrderingList, compareByDate.reversed());
		return notOpenOrderingList;
	}
	
	public Ordering getById(long id)  throws Exception{
		return orderingRepository.findById(id);
	}

	@Transactional
	public Ordering addItems(long orderId, List<Long> itemLongs)  throws Exception{
		
		Ordering ordering = orderingRepository.findById(orderId);
		if(ordering != null) {
			itemLongs.forEach(itemId -> addItem(orderId,itemId));
			ordering.updateTotal();																		// calculate the new total of the order
			applyDiscount(ordering);
			
			return orderingRepository.save(ordering);
		} else {
			return null;
		}

	}	
	
	@Transactional
	public Ordering deleteItems(long orderId, List<Long> itemIds) throws Exception {
		
		Ordering ordering = orderingRepository.findById(orderId);
		if(ordering != null) {
			itemIds.forEach(itemId -> deleteItem(orderId,itemId));
			ordering.updateTotal();																		// calculate the new total of the order
			applyDiscount(ordering);
			
			return orderingRepository.save(ordering);
		} else {
			return null;
		}
		
	}
	
	@Transactional
	public String payOrder(long orderingId, BigDecimal payment) throws Exception {
		boolean orderingExistsAndOpen = orderingRepository.findById(orderingId).getStatus().equalsIgnoreCase("OPEN");
		if(orderingExistsAndOpen) { 												// if the order with such an id exists
			Ordering ordering = orderingRepository.findById(orderingId); 																																			// get the order
			BigDecimal total = ordering.getTotal();
			int result = total.compareTo(payment);
			
			if(result == 0) {
				ordering.setStatus("COMPLETE");
				orderingRepository.save(ordering);
				return "Your order is complete.";
			} else if(result == -1) {
				ordering.setStatus("COMPLETE");
				orderingRepository.save(ordering);
				return "Your order is complete, here is your change: " + payment.subtract(total).toString();
			} else if(result == 1) {
				return "Your order is not complete, you have to insert:" + total.subtract(payment).toString() + " more.";
			} else {
				return "Something went wrong. Try again.";
			}
		} else {
			return "Order does not exist or has already been paid/cancelled.";
		}
	}
	
	
	@Transactional
	public Ordering cancelOrder(long orderingId) throws Exception {
		boolean orderingExistsAndOpen = orderingRepository.findById(orderingId).getStatus().equalsIgnoreCase("OPEN");
		
			if(orderingExistsAndOpen) {
				Ordering ordering = orderingRepository.findById(orderingId); 								// get the order
				ordering.setStatus("CANCELLED");
				return orderingRepository.save(ordering);
			} else {
				return null;
			}
			
		} 
	
	
	@Transactional
	public Ordering addDiscountCodeToOrder(long orderingId, String promotionalCode) throws Exception{
		
		if(promoCodeRepository.existsByCode(promotionalCode)) {
			PromoCode promoCode = promoCodeRepository.findByCode(promotionalCode);
			boolean orderingExistsAndOpen = orderingRepository.findById(orderingId).getStatus().equalsIgnoreCase("OPEN");
			
			if(orderingExistsAndOpen) { 																															// if the order with such an id exists and is open
				Ordering ordering = orderingRepository.findById(orderingId); 																			// get the order
				ordering.setPromoCode(promoCode.getCode());
				ordering.updateTotal();
				applyDiscount(ordering);
				return orderingRepository.save(ordering);
				
			} else {
				return null;
			}
			
		} else {
			return null;
		}
	}
	
	
	@Transactional
	public void applyDiscount(Ordering ordering)  throws Exception{
		if(ordering.getPromoCode() !=null) {
			BigDecimal discount  = promoCodeRepository.findByCode(ordering.getPromoCode()).getDiscount();
			BigDecimal total = ordering.getTotal();
			ordering.setTotal(total.multiply(discount).divide(new BigDecimal("100")));
		}
	}
	
	@Transactional
	public boolean addItem(long orderingId, long itemId) {
		
		boolean orderingExistsAndOpen = orderingRepository.findById(orderingId).getStatus().equalsIgnoreCase("OPEN");
		boolean itemExists = itemRepository.existsById(itemId);
		
		if(orderingExistsAndOpen && itemExists) {
			Ordering ordering = orderingRepository.findById(orderingId);
			Item item = itemRepository.findById(itemId);
			item.increasePopularityByOne();
			boolean orderingItemExists = orderingItemRepository.existsByNameAndOrderingId(item.getName(),ordering.getId());
			
			if(orderingItemExists) {
				OrderingItem orderingItem = orderingItemRepository.findByNameAndOrderingId(item.getName(),ordering.getId());
				orderingItem.increaseQuantityByOne();
				 orderingItemRepository.save(orderingItem);
				return true;
			} else if(!orderingItemExists) {
				OrderingItem newOrderingItem = new OrderingItem(item);
				newOrderingItem.setOrdering(ordering); 											// set the ordering field of the ordering item as the order passed as a parameter
				orderingItemRepository.save(newOrderingItem);								// save it to the repository
				return true;
				
			} else {
				return false;
			}  
			
		} else {
			return false;
		}
	}
	
	@Transactional
	public boolean deleteItem(long orderingId, long itemId) {
		boolean orderingExistsAndOpen = orderingRepository.findById(orderingId).getStatus().equalsIgnoreCase("OPEN");
		boolean itemExists = itemRepository.existsById(itemId);
		
		if(orderingExistsAndOpen && itemExists) {
			Ordering ordering = orderingRepository.findById(orderingId);
			Item item = itemRepository.findById(itemId);
			item.decreasePopularityByOne();
			boolean orderingItemExists = orderingItemRepository.existsByNameAndOrderingId(item.getName(),ordering.getId());
			
			if(orderingItemExists) {
				OrderingItem orderingItem = orderingItemRepository.findByNameAndOrderingId(item.getName(),ordering.getId());
				orderingItem.decreaseQuantityByOne();
				orderingItemRepository.save(orderingItem);
				return true;
				
			} else {
				return false;
			}  
			
		} else {
			return false;
		}
	}

	public Ordering clearOrder(long orderingId) throws Exception {
		boolean orderingExistsAndOpen = orderingRepository.findById(orderingId).getStatus().equalsIgnoreCase("OPEN");
		
		if(orderingExistsAndOpen) {
			Ordering ordering = orderingRepository.findById(orderingId);
			//ordering.setPromoCode(null);
			ordering.getItems().forEach(orderingItem -> {
				Item item = itemRepository.findByName(orderingItem.getName());
				item.decreasePopularityByOne();
				itemRepository.save(item);
				//orderingItemRepository.deleteByIdAndOrderingId(orderingItem.getId(), orderingId);
			});
			ordering.clearOrderingItems();
			ordering.updateTotal();
			return orderingRepository.save(ordering);
		} else {
			return null;
		}
		
	}
	
}
