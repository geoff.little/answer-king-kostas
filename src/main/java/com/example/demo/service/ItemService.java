package com.example.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.entity.Category;
import com.example.demo.dto.InputItemDTO;
import com.example.demo.dto.OutputItemDTO;
import com.example.demo.entity.Item;
import com.example.demo.repository.CategoryRepository;
import com.example.demo.repository.ItemRepository;

@Service
public class ItemService {
	
	@Autowired 
	private ItemRepository itemRepository;
	@Autowired
	private CategoryService categoryService;
	
	@Transactional
	//save a new item in the database
	public OutputItemDTO createItem(InputItemDTO inputItemDTO) throws Exception {
		Item item = new Item(inputItemDTO);
		List<Category> categories = categoryService.getByIds(inputItemDTO.getCategories());
		item.setCategories(categories);
		itemRepository.save(item);
		return new OutputItemDTO(item);
	}
	
		
	//return all items saved in the database
	public List<OutputItemDTO> getAll() throws Exception {
		//put all the items in a list
		//convert them to outputItemDTO
		return toOutput(itemRepository.findAll());
	}


	//get all items sorted by name
	public Collection<OutputItemDTO> getAllSorted()  throws Exception{
		return toOutput( itemRepository.findByOrderByNameAsc());
	}
	
	public Collection<OutputItemDTO> getAllPopularity()  throws Exception{
		return toOutput( itemRepository.findByOrderByPopularityDesc());
	}
	
	
	//get a specific item by id
	public OutputItemDTO getById(long id)  throws Exception{
		if(itemRepository.findById(id) != null) {
			Item item = itemRepository.findById(id);
			return new OutputItemDTO(item);
		} else {
			return null;
		}
	}

	@Transactional
	//update an existing item
	public OutputItemDTO updateItem(long id, InputItemDTO inputItemDTO) throws Exception{
		Item existingItem = itemRepository.findById(id);
		existingItem.setName(inputItemDTO.getName());
		existingItem.setDescription(inputItemDTO.getDescription());
		existingItem.setPrice(inputItemDTO.getPrice());
		existingItem.setCategories(categoryService.getByIds(inputItemDTO.getCategories()));
		itemRepository.save(existingItem);
		return new OutputItemDTO(existingItem);
	}
		
	@Transactional
	//delete an existing item in the database using its id
	public void delete(long id)  throws Exception{
		itemRepository.deleteById(id);
	}
	
	
	protected List<Item> getByIds(List<Long> longs) throws Exception{
		return itemRepository.findAllById(longs);
	}


	private List<OutputItemDTO> toOutput(List<Item> itemList)  throws Exception{
		List <OutputItemDTO> itemDTOList= new ArrayList<OutputItemDTO>();
		for(Item item : itemList) {
			itemDTOList.add(new OutputItemDTO(item));
		}
		return itemDTOList;
	}
	
	public Item getItemById(long id) throws Exception {
		return itemRepository.findById(id);
	}


	
	
}
