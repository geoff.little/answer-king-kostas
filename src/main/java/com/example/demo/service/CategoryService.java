package com.example.demo.service;
import java.util.ArrayList;

import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.dto.InputCategoryDTO;
import com.example.demo.dto.OutputCategoryDTO;
import com.example.demo.dto.OutputItemDTO;
import com.example.demo.entity.Category;
import com.example.demo.entity.Item;
import com.example.demo.repository.CategoryRepository;
import com.example.demo.repository.ItemRepository;


@Service
public class CategoryService {
	
	@Autowired 
	private CategoryRepository categoryRepository;
	@Autowired
	private ItemService itemService;
	@Autowired
	private ItemRepository itemRepository;;
	
	@Transactional
	//save a new category in the database
	public OutputCategoryDTO createCategory(InputCategoryDTO inputCategoryDTO)  throws Exception{
		
		Category category = new Category(inputCategoryDTO);
		List<Item> items = itemService.getByIds(inputCategoryDTO.getItems());		
		category.setItems(items);
		
		categoryRepository.save(category);
		return new OutputCategoryDTO(category);
	}
			
	
	//return all categories saved in the database
	public List<OutputCategoryDTO> getAll()  throws Exception{
		
		return toOutput(categoryRepository.findAll());
	
	}
	
	
	//get all items sorted by their name
	public List<OutputCategoryDTO> getAllSorted()  throws Exception{
		
		return toOutput(categoryRepository.findByOrderByNameAsc());
		
	}
	
	
	//get a specific category by id
	public OutputCategoryDTO getById(long id)  throws Exception{
		if(categoryRepository.findById(id) != null) {
				Category category = categoryRepository.findById(id);
				return new OutputCategoryDTO(category);
		} else {
			return null;
		}
	}
	

	//update an existing category 
	@Transactional
	public OutputCategoryDTO updateCategory(long id, InputCategoryDTO inputCategoryDTO) throws Exception {
		
		if(categoryRepository.findById(id) != null) {
			Category existingCategory = categoryRepository.getOne(id);
			existingCategory.setName(inputCategoryDTO.getName());
			existingCategory.setDescription(inputCategoryDTO.getDescription());
			
			List<Item>existingCategoryItems =  itemService.getByIds(existingCategory.getItemIds());
			existingCategoryItems.forEach(item -> {
				item.removeCategory(existingCategory);
				itemRepository.save(item);
			});
			
			existingCategory.setItems(itemService.getByIds(inputCategoryDTO.getItems()));
			
			if(inputCategoryDTO.getItems() != null) {
				List<Long> itemIds = inputCategoryDTO.getItems();
				for(Long itemId : itemIds) {
					if(itemService.getById(itemId) != null) {
						Item item = itemService.getItemById(itemId.longValue());
						item.addCategory(existingCategory);
						itemRepository.save(item);
					}
				}
			}
			
			categoryRepository.save(existingCategory);

			return new OutputCategoryDTO(existingCategory);
		} else {
			return null;
		}
		
	}
	
	@Transactional
	//delete an existing item in the database using its id
	public void delete(long id)  throws Exception{
		categoryRepository.deleteById(id);
	}
		
	
	public List<Category> getByIds(List<Long> longs) throws Exception{
		return categoryRepository.findAllById(longs);
	}
	
	
	public List<OutputCategoryDTO> toOutput(List<Category> categories) throws Exception{
		List<OutputCategoryDTO> outputCategories = new ArrayList<OutputCategoryDTO>();
		for(Category category: categories) {
			outputCategories.add(new OutputCategoryDTO(category));
		}
		return outputCategories;
	}
	
	
}
