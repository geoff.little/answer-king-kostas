package com.example.demo.dto;

import java.math.BigDecimal;
import java.util.List;

import com.example.demo.entity.Category;
import com.example.demo.entity.Item;

public class OutputItemDTO {
	
	private long id;
	private String name;
	private String description;
	private BigDecimal price;
	private List<Long> categories;
	private int popularity;
	
	public OutputItemDTO(Item item) {
		this.id = item.getId();
		this.name = item.getName();
		this.description = item.getDescription();
		this.price = item.getPrice();
		this.categories = item.getCategoryIds();
		this.popularity = item.getPopularity();
	}
	
	
	public OutputItemDTO(long id, String name, String description, BigDecimal price, List<Long> categories) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.categories = categories;
	}


	public int getPopularity() {
		return popularity;
	}


	public void setPopularity(int popularity) {
		this.popularity = popularity;
	}


	public long getId() {return id;}
	public void setId(long id) {this.id = id;}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public BigDecimal getPrice() {
		return price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public List<Long> getCategories() {
		return categories;
	}


	public void setCategories(List<Long> categories) {
		this.categories = categories;
	}
	
}
