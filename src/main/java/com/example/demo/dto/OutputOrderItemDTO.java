package com.example.demo.dto;

import java.math.BigDecimal;
import java.util.List;

import com.example.demo.entity.Category;
import com.example.demo.entity.OrderingItem;

public class OutputOrderItemDTO {
	
	
	private long id;
	private String name;
	private String description;
	private BigDecimal price;
	
	
	public OutputOrderItemDTO() {
		
	}
	
	public OutputOrderItemDTO(OrderingItem orderItem) {
		this.id = orderItem.getId();
		this.name = orderItem.getName();
		this.description = orderItem.getDescription();
		this.price = orderItem.getPrice();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
}
