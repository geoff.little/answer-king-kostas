package com.example.demo.dto;

import java.util.List;

import com.example.demo.entity.Category;

public class OutputCategoryDTO {
	
	private long id;
	private String name;
	private String description;
	private List<Long> items;
	
	public OutputCategoryDTO(Category category) {
		this.id = category.getId();
		this.name = category.getName();
		this.description = category.getDescription();
		this.items = category.getItemIds();
	}
	
	public OutputCategoryDTO(long id, String name, String description, List<Long> items) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.items = items;
	}
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<Long> getItems() {
		return items;
	}
	public void setItems(List<Long> items) {
		this.items = items;
	}
	
	
	
}
