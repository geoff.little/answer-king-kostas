package com.example.demo.dto;

import java.math.BigDecimal;
import java.util.List;


public class InputItemDTO {
	
	private String name;
	private String description;
	private BigDecimal price;
	private List<Long> categories;
	
	public InputItemDTO() {
		
	}
	
	public InputItemDTO(String name, String description, BigDecimal price, List<Long> categories) {
		this.name = name;
		this.description = description;
		this.price = price;
		this.categories = categories;
	}

	public List<Long> getCategories() {return categories;}
	public void setCategories(List<Long> categories) {this.categories = categories;}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public BigDecimal getPrice() {
		return price;
	}



	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
}
