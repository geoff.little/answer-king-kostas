package com.example.demo.dto;

import java.math.BigDecimal;
import java.util.List;

public class InputCategoryDTO {
	
	private String name;
	private String description;
	private List<Long> items;
	
	public InputCategoryDTO(String name, String description, List<Long> items) {
		this.name = name;
		this.description = description;
		this.items = items;
	}
	
	public List<Long> getItems() {return items;}
	public void setItems(List<Long> items) {this.items = items;}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
