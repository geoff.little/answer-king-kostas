package com.example.demo.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.example.demo.dto.InputCategoryDTO;

@Entity
public class Category {
	
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Id private long id;
	private String name;
	private String description;
	
	@ManyToMany(mappedBy = "categories", cascade = { CascadeType.ALL })
	private List<Item> items;
	
	public Category() {
		
	}
	
	public Category(InputCategoryDTO inputCategoryDTO) {
		this.name = inputCategoryDTO.getName();
		this.description = inputCategoryDTO.getDescription();
	};
	

	public Category(String name, String description, List<Item> items) {
		this.name = name;
		this.description = description;
		this.items = items;
	}

	public long getId() {return id;}
	public void setId(long id) {this.id = id;}
	
	public List<Item> getItems() {return items;}
	public void setItems(List<Item> items) {this.items = items;}


	public List<Long> getItemIds() {
		List<Long> longList = new ArrayList<Long>();
		for(Item item: items) {
			longList.add(item.getId());
		}
		return longList;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
	
}
