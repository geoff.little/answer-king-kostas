package com.example.demo.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.JoinColumn;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import com.example.demo.dto.InputItemDTO;
import com.example.demo.dto.OutputCategoryDTO;

@Entity
public class Item  {
	
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Id private long id;
	private String name;
	private String description;
	private BigDecimal price;
	private int popularity;
	@ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
        name = "item_category", 
        joinColumns = { @JoinColumn(name = "item_id") }, 
        inverseJoinColumns = { @JoinColumn(name = "category_id") }
    )
	private List<Category> categories;
	
	public Item() {
		
	}
	
	public Item(InputItemDTO inputItemDTO) {
		this.name = inputItemDTO.getName();
		this.description = inputItemDTO.getDescription();
		this.price = inputItemDTO.getPrice();
		this.popularity = 0;
	}

	public Item(String name, String description, BigDecimal price, List<Category> categories) {
		this.name = name;
		this.description = description;
		this.price = price;
		this.categories = categories;
		this.popularity = 0;
	}
	
	public long getId() {return id;}
	public void setId(long id) {this.id = id;}
	
	public List<Category> getCategories() {return categories;}
	public void setCategories(List<Category> categories) {this.categories = categories;}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
	public List<Long> getCategoryIds(){
		
		List<Long> longList = new ArrayList<Long>();
		for(Category category:categories) {
			longList.add(category.getId());
		}
		
		return longList;
	}
	
	public List<OutputCategoryDTO> getOutputCategoryDTOs(){
		
		List<OutputCategoryDTO> outputCategoryDTOList = new ArrayList<OutputCategoryDTO>();
		for(Category category:categories) {
			outputCategoryDTOList.add(new OutputCategoryDTO(category));
		}
		
		return outputCategoryDTOList;
	}

	public int getPopularity() {
		return popularity;
	}

	public void setPopularity(int popularity) {
		this.popularity = popularity;
	}
	
	public void increasePopularityByOne() {
		popularity++;
	}

	public void decreasePopularityByOne() {
		popularity--;
	}
	
	public void addCategory(Category category) {
		categories.add(category);
	}
	
	public void removeCategory(Category category) {
		categories.remove(category);
	}
	
	
}
