package com.example.demo.entity;

import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
public class OrderingItem {

	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Id private long id;
	private String name;
	private String description;
	private BigDecimal price;
	@ManyToOne
	@JoinColumn(name = "ordering_id")
	@JsonIgnore
	private Ordering ordering;
	private BigDecimal sub;
	private int quantity;
	
	public OrderingItem() {
		
	}

	public OrderingItem(Item item) {
		this.name = item.getName();
		this.description = item.getDescription();
		this.price = item.getPrice();
		this.quantity = 1;
		this.sub = price;
	}
	
	public void updateQuantity() {
		if(this.quantity <= 0) {
			this.quantity = 0;
		}
	}
	
	public void updateSub() {
		updateQuantity();
		this.sub = price.multiply(new BigDecimal(Integer.valueOf(quantity)));
		if(this.sub.compareTo(BigDecimal.ZERO) == 0) {
			this.sub = BigDecimal.ZERO;
		}
	}
	
	public void increaseQuantityByOne() {
		quantity++;
	}
	
	public void decreaseQuantityByOne() {
		quantity--;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	
	public Ordering getOrdering() {
		return ordering;
	}

	public void setOrdering(Ordering ordering) {
		this.ordering = ordering;
	}

	public BigDecimal getSub() {
		updateSub();
		return sub;
	}

	public void setSub(BigDecimal sub) {
		this.sub = sub;
	}
	
}
