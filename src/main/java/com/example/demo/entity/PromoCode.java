package com.example.demo.entity;

import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class PromoCode {
	
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Id private long id;
	private String code;
	private BigDecimal discount;
	
	public PromoCode() {
		
	}
	
	public PromoCode(String code, BigDecimal discount) {
		this.code = code;
		this.discount = discount;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	
}
