package com.example.demo.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import com.fasterxml.jackson.annotation.JsonFormat;


@Entity
public class Ordering {
	
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Id private long id;
	private String status;
	private BigDecimal total;
	@OneToMany(mappedBy = "ordering")
	private List<OrderingItem> orderingItems;
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
	private LocalDateTime localDateTime;
	private String promoCode;
	
	public Ordering() {
		
	}
	

	public Ordering(LocalDateTime localDateTime, BigDecimal total, String status) {
		this.localDateTime = localDateTime;
		this.total = total;
		this.status = status;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}

	public List<OrderingItem> getItems() {
		return orderingItems;
	}

	public void setItems(List<OrderingItem> orderItems) {
		this.orderingItems = orderItems;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public BigDecimal getTotal() {
		return total;
	}


	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	
	public void add(OrderingItem orderingItem) {
		orderingItems.add(orderingItem);
	}
	
	public void remove(OrderingItem orderingItem) {
		orderingItems.remove(orderingItem);
	}
		
	
	public void updateTotal() {
		this.total = new BigDecimal("0");
		if(orderingItems != null) {
			orderingItems.forEach(item -> this.total = this.total.add(item.getSub()));
		}
		
	}


	public String getPromoCode() {
		return promoCode;
	}


	public void setPromoCode(String  promoCode) {
		this.promoCode = promoCode;
	}
	
	public void clearOrderingItems(){
		orderingItems.clear();
		updateTotal();
	}


	public LocalDateTime getLocalDateTime() {
		return localDateTime;
	}


	public void setLocalDateTime(LocalDateTime localDateTime) {
		this.localDateTime = localDateTime;
	}
	
	
	
}
