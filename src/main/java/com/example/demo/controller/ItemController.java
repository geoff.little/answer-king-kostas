package com.example.demo.controller;

import java.util.Collection;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.InputItemDTO;
import com.example.demo.dto.OutputItemDTO;
import com.example.demo.entity.Item;
import com.example.demo.service.ItemService;

@RestController()
@RequestMapping("/item")
public class ItemController {

	@Autowired
	private ItemService itemService;
	
	@PostMapping
	public @ResponseBody OutputItemDTO createItem(@Valid @RequestBody InputItemDTO createItemDTO) throws Exception{
		return itemService.createItem(createItemDTO);
	}
	
	@GetMapping
	public @ResponseBody Collection<OutputItemDTO> getItems()  throws Exception{
		return itemService.getAll();
	}
	
	@GetMapping("/absorted")
	public @ResponseBody Collection<OutputItemDTO> getItemsSorted()  throws Exception{
		return itemService.getAllSorted();
	}
	
	@GetMapping("/popular")
	public @ResponseBody Collection<OutputItemDTO> getPopularItems()  throws Exception{
		return itemService.getAllPopularity();
	}
	
	@GetMapping("/{id}")
	public @ResponseBody OutputItemDTO getItemById(@PathVariable long id)  throws Exception{
		return itemService.getById(id);
	}
	
	
	@PutMapping("/{id}")
	public @ResponseBody OutputItemDTO updateItem(@PathVariable long id, @RequestBody InputItemDTO inputItemDTO) throws Exception{
	    return itemService.updateItem(id,inputItemDTO);
	}
	
	@DeleteMapping("/{id}")
	void delete(@PathVariable long id) throws Exception{
		itemService.delete(id);
	}
}
