package com.example.demo.controller;

import java.util.Collection;
import java.util.List;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.dto.InputCategoryDTO;
import com.example.demo.dto.OutputCategoryDTO;
import com.example.demo.dto.OutputItemDTO;
import com.example.demo.entity.Category;
import com.example.demo.entity.Item;
import com.example.demo.service.CategoryService;
import com.example.demo.service.ItemService;


@RestController()
@RequestMapping("/category")
public class CategoryController {

	@Autowired
	private CategoryService categoryService;
	
	@PostMapping
	public @ResponseBody OutputCategoryDTO createCategory(@Valid @RequestBody InputCategoryDTO inputCategoryDTO) throws Exception{
		return categoryService.createCategory(inputCategoryDTO);
	}
	
	@GetMapping
	public @ResponseBody List<OutputCategoryDTO> getCategories() throws Exception{

		return categoryService.getAll();
	}
	
	@GetMapping("/sorted")
	public @ResponseBody List<OutputCategoryDTO> getCategoriesSorted() throws Exception{

		return categoryService.getAllSorted();
	}
	
	@GetMapping("/{id}")
	public @ResponseBody OutputCategoryDTO getCategoryById(@PathVariable long id) throws Exception{

		return categoryService.getById(id);
	}
	
	@PutMapping("/{id}")
	public @ResponseBody OutputCategoryDTO updateCategory(@PathVariable long id, @RequestBody InputCategoryDTO inputCategoryDTO)  throws Exception{
	    return categoryService.updateCategory(id,inputCategoryDTO);
	}
	
	@DeleteMapping("/{id}")
	void delete(@PathVariable long id) throws Exception{
		categoryService.delete(id);
	}
}
