package com.example.demo.controller;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.entity.Ordering;
import com.example.demo.service.OrderingService;
import com.fasterxml.jackson.annotation.JsonFormat;

@RestController()
@RequestMapping("/ordering")
public class OrderController {
	
	@Autowired
	private OrderingService orderingService;
	
	@PostMapping
	public @ResponseBody Ordering createOrder()   throws Exception{
		return orderingService.createOrder();
	}
	
	@GetMapping
	public @ResponseBody Collection<Ordering> getOrders() throws Exception{
		return orderingService.getAll();
	}
	
	@GetMapping("/history")
	public @ResponseBody Collection<Ordering> getOrderHistory(@RequestParam @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate startDate,@RequestParam @DateTimeFormat(pattern = "dd/MM/yyyy")  LocalDate endDate) throws Exception{
		return orderingService.getOrderHistory(startDate,endDate);
	}
	
	@GetMapping("/{id}")
	public @ResponseBody Ordering getOrderById(@PathVariable long id) throws Exception{
		return orderingService.getById(id);
	}
	
	@PutMapping("/add/{id}")
	public @ResponseBody Ordering addToOrder(@PathVariable long id, @RequestBody List<Long> longs)  throws Exception{
	    return orderingService.addItems(id,longs);
	}
	
	@PutMapping("/delete/{id}")
	public @ResponseBody Ordering deleteFromOrder(@PathVariable long id, @RequestBody List<Long> longs)  throws Exception{
	    return orderingService.deleteItems(id,longs);
	}
	
	@PutMapping("/pay/{id}")
	public @ResponseBody String payOrder(@PathVariable long id, @RequestParam BigDecimal payment)  throws Exception{
	    return orderingService.payOrder(id,payment);
	}
	
	@PutMapping("/cancel/{id}")
	public @ResponseBody Ordering cancelOrder(@PathVariable long id) throws Exception {
	    return orderingService.cancelOrder(id);
	}
	
//	@PutMapping("/clear/{id}")
//	public @ResponseBody Ordering clearOrder(@PathVariable long id) throws Exception {
//	    return orderingService.clearOrder(id);
//	}
	
	@PutMapping("/discount/{id}")
	public @ResponseBody Ordering addCode(@PathVariable long id,@RequestParam String promotionalCode) throws Exception {
	    return orderingService.addDiscountCodeToOrder(id,promotionalCode);
	}
	
}
