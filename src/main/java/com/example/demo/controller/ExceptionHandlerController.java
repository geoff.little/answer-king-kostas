package com.example.demo.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import com.example.demo.util.ErrorMessage;

import javax.validation.ValidationException;

import org.springframework.http.HttpStatus;


@ControllerAdvice
public class ExceptionHandlerController {
	
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler
	ErrorMessage exceptionHandler(ValidationException e) {
		return new ErrorMessage("400",e.getMessage());
	}
	
//	@ResponseBody
//	@ResponseStatus(HttpStatus.BAD_REQUEST)
//	@ExceptionHandler
//	ErrorMessage exceptionHandler(Exception e) {
//		return new ErrorMessage("500",e.getMessage());
//	}
}
