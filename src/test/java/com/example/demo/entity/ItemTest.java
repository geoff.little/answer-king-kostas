package com.example.demo.entity;

import static org.junit.Assert.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class ItemTest {

	@Test
	public void testItemStringStringBigDecimalListOfCategory() {
		Item item = new Item( "bean", "this is a bean", new BigDecimal("1.99"), null );
		assertNotEquals(null,item.getClass());
	}

	@Test
	public void testGetId() {
		Item item = new Item( "bean", "this is a bean", new BigDecimal("1.99"), null );
		assertEquals(0,item.getId());
	}

	@Test
	public void testSetId() {
		Item item = new Item( "bean", "this is a bean", new BigDecimal("1.99"), null );
		item.setId(1);
		assertEquals(1,item.getId());
	}

	@Test
	public void testGetName() {
		Item item = new Item( "bean", "this is a bean", new BigDecimal("1.99"), null );
		assertEquals("bean",item.getName());
	}

	@Test
	public void testSetName() {
		Item item = new Item( "bean", "this is a bean", new BigDecimal("1.99"), null );
		item.setName("notBean");
		assertEquals("notBean",item.getName());
	}

	@Test
	public void testGetDescription() {
		Item item = new Item( "bean", "this is a bean", new BigDecimal("1.99"), null );
		assertEquals("this is a bean",item.getDescription());
	}

	@Test
	public void testSetDescription() {
		Item item = new Item( "bean", "this is a bean", new BigDecimal("1.99"), null );
		item.setDescription("this is not a bean");
		assertEquals("this is not a bean",item.getDescription());
	}

	@Test
	public void testGetPrice() {
		Item item = new Item( "bean", "this is a bean", new BigDecimal("1.99"), null );
		
		System.out.println(item.getPrice());
		System.out.println(item.getName());
		System.out.println(item.getDescription());
		
		assertEquals(new BigDecimal("1.99"),item.getPrice());
		
	}

	@Test
	public void testSetPrice() {
		Item item = new Item( "bean", "this is a bean", new BigDecimal("1.99"), null );
		item.setPrice(new BigDecimal("9.99"));
		assertEquals(new BigDecimal("9.99"),item.getPrice());
	}

	@Test
	public void testGetCategories() {
		Item item = new Item("bean", "this is a bean", new BigDecimal(1.99), null );
		Item item2 = new Item("bread", "this is a bread", new BigDecimal(6.99), null );
		
		List<Item> items = new ArrayList<Item>();
		items.add(item);
		items.add(item2);
		Category category = new Category("pizza", "this is a pizza category", items);
		List<Category> categories = new ArrayList<Category>();
		categories.add(category);
		
		
		Item itemTest = new Item("test", "this is a test", new BigDecimal(1.99), categories);
		
		assertNotEquals(null,itemTest.getCategories());
		assertEquals(categories,itemTest.getCategories());
	}

	@Test
	public void testSetCategories() {
		Item item = new Item("bean", "this is a bean", new BigDecimal(1.99), null );
		Item item2 = new Item("bread", "this is a bread", new BigDecimal(6.99), null );
		
		List<Item> items = new ArrayList<Item>();
		items.add(item);
		items.add(item2);
		Category category = new Category("pizza", "this is a pizza category", items);
		List<Category> categories = new ArrayList<Category>();
		categories.add(category);
		
		Category category2 = new Category("second pizza", "this is a second pizza category", items);
		List<Category> categories2 = new ArrayList<Category>();
		categories.add(category2);
		
		Item itemTest = new Item("test", "this is a test", new BigDecimal(1.99), categories);
		itemTest.setCategories(categories2);
		assertNotEquals(null,itemTest.getCategories());
		assertEquals(categories2,itemTest.getCategories());
	}

}
